# Web BFF

This is a microsevice. Used as component of Hospital Registry application with basic hospital registry functionality.
<br/>
The microsevice is an application of BFF pattern for the web client.

Running application link: http://194.31.174.36:9099/
<br/>
DockerHub - https://hub.docker.com/repositories/rloutsker

## Application architecture
![Application architecture](https://gitlab.com/devsphere/hospital-registry/authentication-server/-/raw/master/HospitalRegistry-SystemArchitecture.png)
