package com.javapadawan.WebBFF.controller;

import com.javapadawan.WebBFF.entity.payload.DoctorDto;
import com.javapadawan.WebBFF.entity.payload.PatientDto;
import com.javapadawan.WebBFF.entity.payload.ReferralDto;
import com.javapadawan.WebBFF.service.logic.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/web/ui/domain")
public class DomainController {
    private final DomainService service;

    @Autowired
    public DomainController(DomainService service) {
        this.service = service;
    }

    @GetMapping("/referral/onPatient/{email}")
    public Object[] getReferralsOnPatientEmail(@PathVariable String email,
                                        @RequestHeader(name = "Authorization") String authHeader) {
        service.updateAuthHeader(authHeader);
        try {
            return service.getReferralOnPatientEmail(email);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/referral/onDoctor/{email}")
    public Object[] getReferralsOnDoctorEmail(@PathVariable String email,
                                        @RequestHeader(name = "Authorization") String authHeader) {
        service.updateAuthHeader(authHeader);
        try {
            return service.getReferralOnDoctorEmail(email);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping("/referral/save")
    public void addReferral(@RequestBody ReferralDto dto,
                            @RequestHeader(name = "Authorization") String authHeader) {
        service.updateAuthHeader(authHeader);
        try {
            service.addReferral(dto);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @DeleteMapping("/referral/delete")
    public void deleteReferral(@RequestBody ReferralDto dto,
                               @RequestHeader(name = "Authorization") String authHeader) {
        service.updateAuthHeader(authHeader);
        service.deleteReferral(dto);
    }

    @PostMapping("/doctor/save")
    public void addDoctor(@RequestBody DoctorDto dto,
                          @RequestHeader(name = "Authorization") String authHeader) {
        service.updateAuthHeader(authHeader);
        try {
            service.addDoctor(dto);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @DeleteMapping("/doctor/delete/{email}")
    public void deleteDoctor(@PathVariable String email,
                             @RequestHeader(name = "Authorization") String authHeader) {
        service.updateAuthHeader(authHeader);
        try {
            service.deleteDoctorByEmail(email);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping("/doctor/bySample")
    public Object[] getDoctorsBySample(@RequestBody DoctorDto dto,
                                       @RequestHeader(name = "Authorization") String authHeader) {
        service.updateAuthHeader(authHeader);
        return service.getDoctorsBySample(dto);
    }

    @GetMapping("/doctor/all")
    public Object[] getAllDoctors() {
        try {
            return service.getAllDoctors();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "meh :-(");
        }
    }

    @PostMapping("/patient/save")
    public void addPatient(@RequestBody PatientDto dto,
                           @RequestHeader(name = "Authorization") String authHeader) {
        service.updateAuthHeader(authHeader);
        try {
            service.addPatient(dto);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
