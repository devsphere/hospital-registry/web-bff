package com.javapadawan.WebBFF.controller;

import com.javapadawan.WebBFF.entity.domain.Credential;
import com.javapadawan.WebBFF.entity.domain.RegistryUser;
import com.javapadawan.WebBFF.exception.DomainException;
import com.javapadawan.WebBFF.service.auth.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/web/ui/security")
public class AuthController {
    private final AuthService service;

    @Autowired
    public AuthController(AuthService service) {
        this.service = service;
    }

    @PostMapping("/authentication")
    public ResponseEntity<RegistryUser> authenticate(@RequestBody Credential credential) {
        try {
            return service.authenticateCredentials(credential);
        } catch (DomainException ex) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
        }
    }
}
