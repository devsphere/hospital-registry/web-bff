package com.javapadawan.WebBFF.entity.domain;

public class RegistryUser {
    private String token;
    private String role;
    Object person;

    public RegistryUser() {
    }

    public RegistryUser(String role, Object person, String token) {
        this.role = role;
        this.person = person;
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Object getPerson() {
        return person;
    }

    public void setPerson(Object person) {
        this.person = person;
    }
}
