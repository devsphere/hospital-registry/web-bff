package com.javapadawan.WebBFF.service;

import com.javapadawan.WebBFF.entity.payload.AuthDto;
import com.javapadawan.WebBFF.entity.domain.Credential;
import com.javapadawan.WebBFF.entity.payload.DoctorDto;
import com.javapadawan.WebBFF.entity.payload.PatientDto;
import com.javapadawan.WebBFF.entity.payload.ReferralDto;
import com.javapadawan.WebBFF.exception.DomainException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Service
public class WebClient implements AuthRequester, BusinessRequester {
    private final RestTemplate rest;
    private final String authServerAddress;
    private final String domainServerAddress;
    private HttpHeaders tokenHeader;

    @Autowired
    public WebClient(@Value("${app.authentication-server.url}") String authServerUrl,
                     @Value("${app.authentication-server.port}") String authServerPort,
                     @Value("${app.domain-server.url}") String domainServerUrl,
                     @Value("${app.domain-server.port}") String domainServerPort) {
        this.rest = new RestTemplate();
        this.domainServerAddress = domainServerUrl + domainServerPort;
        this.authServerAddress = authServerUrl + authServerPort;
        tokenHeader = new HttpHeaders();
        tokenHeader.add("Authorization", "");
    }

    //auth methods
    public void updateAuthHeader(String headerValue) {
        tokenHeader.set("Authorization", headerValue);
    }

    public ResponseEntity<AuthDto> authenticate(Credential credential) {
        try {
            return rest.exchange(
                    authServerAddress + "/authentication/credentials",
                    HttpMethod.POST,
                    new HttpEntity<>(credential),
                    AuthDto.class
            );
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getMessageFromAuthDtoException(ex));
        }
    }

    public Object requestAuthenticatedUser(AuthDto authDto) {
        ResponseEntity<Object> response = rest.exchange(
                domainServerAddress + "/authentication/byToken",
                HttpMethod.POST,
                new HttpEntity<>(authDto, tokenHeader),
                Object.class
        );
        return response.getBody();
    }

    //logic methods
    public Object[] requestAllDoctors() {
        ResponseEntity<Object[]> response = rest.exchange(
                domainServerAddress + "/public/staff/doctors",
                HttpMethod.GET,
                new HttpEntity<>(tokenHeader),
                Object[].class
        );
        return response.getBody();
    }

    public void addReferral(ReferralDto referralDto) {
        try {
            rest.exchange(
                    domainServerAddress + "/logic/referral/save",
                    HttpMethod.POST,
                    new HttpEntity<>(referralDto, tokenHeader),
                    Void.class
            );
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getMessageFromStringException(ex));
        }
    }

    public void deleteReferral(ReferralDto referralDto) {
        rest.exchange(
                domainServerAddress + "/logic/referral/delete",
                HttpMethod.DELETE,
                new HttpEntity<>(referralDto, tokenHeader),
                Void.class
        );
    }

    public void addDoctor(DoctorDto doctorDto) {
        try {
            rest.exchange(
                    domainServerAddress + "/logic/doctor/save",
                    HttpMethod.POST,
                    new HttpEntity<>(doctorDto, tokenHeader),
                    Void.class
            );
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getMessageFromStringException(ex));
        }
    }

    public void deleteDoctor(String email) {
        try {
            ResponseEntity<Void> response = rest.exchange(
                    domainServerAddress + "/logic/doctor/delete/" + email,
                    HttpMethod.DELETE,
                    new HttpEntity<>(tokenHeader),
                    Void.class
            );
        }  catch (HttpStatusCodeException ex) {
            throw new DomainException(getMessageFromStringException(ex));
        }
    }

    public Object[] requestDoctorsBySample(DoctorDto doctorDto) {
        ResponseEntity<Object[]> response = rest.exchange(
                domainServerAddress + "/logic/doctor/bySample",
                HttpMethod.POST,
                new HttpEntity<>(doctorDto, tokenHeader),
                Object[].class
        );
        return response.getBody();
    }

    public Object[] requestReferralsOnPatientEmail(String email) {
        ResponseEntity<Object[]> response = rest.exchange(
                domainServerAddress + "/logic/referral/onPatient/" + email,
                HttpMethod.GET,
                new HttpEntity<>(tokenHeader),
                Object[].class
        );
        return response.getBody();
    }

    public Object[] requestReferralsOnDoctorEmail(String email) {
        ResponseEntity<Object[]> response = rest.exchange(
                domainServerAddress + "/logic/referral/onDoctor/" + email,
                HttpMethod.GET,
                new HttpEntity<>(tokenHeader),
                Object[].class
        );
        return response.getBody();
    }

    public void addPatient(PatientDto patientDto) {
        try {
            rest.exchange(
                    domainServerAddress + "/logic/patient/save",
                    HttpMethod.POST,
                    new HttpEntity<>(patientDto, tokenHeader),
                    Void.class
            );
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getMessageFromStringException(ex));
        }
    }

    //private methods
    private String getMessageFromAuthDtoException(HttpStatusCodeException ex) {
        return ex.getResponseBodyAs(AuthDto.class).getMessage();
    }

    private String getMessageFromStringException(HttpStatusCodeException ex) {
        return ex.getResponseBodyAs(String.class);
    }
}
