package com.javapadawan.WebBFF.service.logic;

import com.javapadawan.WebBFF.entity.payload.AuthDto;
import com.javapadawan.WebBFF.entity.payload.DoctorDto;
import com.javapadawan.WebBFF.entity.payload.PatientDto;
import com.javapadawan.WebBFF.entity.payload.ReferralDto;
import com.javapadawan.WebBFF.exception.DomainException;
import com.javapadawan.WebBFF.service.BusinessRequester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

@Service
public class DomainService {
    private final BusinessRequester requester;

    @Autowired
    public DomainService(BusinessRequester requester) {
        this.requester = requester;
    }

    public void updateAuthHeader(String headerValue) {
        requester.updateAuthHeader(headerValue);
    }

    public Object[] getAllDoctors() {
        try {
            return requester.requestAllDoctors();
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getErrorMessageFromException(ex));
        }
    }

    public Object[] getReferralOnPatientEmail(String email) {
        try {
            return requester.requestReferralsOnPatientEmail(email);
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getErrorMessageFromException(ex));
        }
    }

    public void addReferral(ReferralDto dto) {
        requester.addReferral(dto);
    }

    public void deleteReferral(ReferralDto dto) {
        try {
            requester.deleteReferral(dto);
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getErrorMessageFromException(ex));
        }
    }

    public void addDoctor(DoctorDto dto) {
        requester.addDoctor(dto);
    }

    public void deleteDoctorByEmail(String email) {
        try {
            requester.deleteDoctor(email);
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getErrorMessageFromException(ex));
        }
    }

    public Object[] getDoctorsBySample(DoctorDto dto) {
        try {
            return requester.requestDoctorsBySample(dto);
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getErrorMessageFromException(ex));
        }
    }

    public Object[] getReferralOnDoctorEmail(String email) {
        try {
            return requester.requestReferralsOnDoctorEmail(email);
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getErrorMessageFromException(ex));
        }
    }

    public void addPatient(PatientDto dto) {
        try {
            requester.addPatient(dto);
        } catch (HttpStatusCodeException ex) {
            throw new DomainException(getErrorMessageFromException(ex));
        }
    }

    private String getErrorMessageFromException(HttpStatusCodeException ex) {
        return ex.getResponseBodyAs(AuthDto.class).getMessage();
    }
}
