package com.javapadawan.WebBFF.service;

import com.javapadawan.WebBFF.entity.payload.DoctorDto;
import com.javapadawan.WebBFF.entity.payload.PatientDto;
import com.javapadawan.WebBFF.entity.payload.ReferralDto;

public interface BusinessRequester {
    Object[] requestAllDoctors();
    void addReferral(ReferralDto referralDto);
    void deleteReferral(ReferralDto referralDto);
    Object[] requestReferralsOnPatientEmail(String email);
    Object[] requestReferralsOnDoctorEmail(String email);
    void updateAuthHeader(String headerValue);
    void addDoctor(DoctorDto doctorDto);
    void deleteDoctor(String email);
    Object[] requestDoctorsBySample(DoctorDto doctorDto);
    void addPatient(PatientDto patientDto);
}
