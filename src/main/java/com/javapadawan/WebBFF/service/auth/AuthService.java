package com.javapadawan.WebBFF.service.auth;

import com.javapadawan.WebBFF.entity.domain.Credential;
import com.javapadawan.WebBFF.entity.domain.RegistryUser;
import com.javapadawan.WebBFF.entity.payload.AuthDto;
import com.javapadawan.WebBFF.service.AuthRequester;
import com.javapadawan.WebBFF.service.WebClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    private final AuthRequester requester;

    @Autowired
    public AuthService(WebClient requester) {
        this.requester = requester;
    }

    public ResponseEntity<RegistryUser> authenticateCredentials(Credential credential) {
        ResponseEntity<AuthDto> response = requester.authenticate(credential);
        AuthDto dto = response.getBody();
        HttpHeaders headers = response.getHeaders();
        Object person = requester.requestAuthenticatedUser(dto);
        String role = dto.getRole();
        String token = headers.getFirst("Authorization");
        RegistryUser user = new RegistryUser(role, person, token);
        return new ResponseEntity<>(
                user,
                headers,
                HttpStatus.OK
        );
    }
}
