package com.javapadawan.WebBFF.service;

import com.javapadawan.WebBFF.entity.domain.Credential;
import com.javapadawan.WebBFF.entity.payload.AuthDto;
import org.springframework.http.ResponseEntity;

public interface AuthRequester {
    ResponseEntity<AuthDto> authenticate(Credential credential);
    Object requestAuthenticatedUser(AuthDto authDto);
}
